# Tuto pour créer un site web avec hugo déployé automatiquement sur gitlab-pages
Voici un petit tuto en français, à la fraiche du matin, il faut compter moins de 30 min.


Mon dépôt de ce tuto sur framagit : https://framagit.org/mxbossard/test-hugo/
Site démo réalisé avec ce tuto ce matin : https://mxbossard.frama.io/test-hugo/


## Pour tester sur son pc, avec un terminal
1. Tu commence par installer git si tu ne l'a pas déjà : sudo apt-get install git
1. Ensuite tu clone le dépôt avec : git clone https://gitlab.com/pages/hugo.git
1. Là, il faut s'accrocher, c'est probablement le plus compliqué, installer hugo (pleins de possibilités), je propose la méthode officiel : https://gohugo.io/getting-started/installing/
1. Tu entre dans ce répertoire du projet git avec la commande : cd hugo
1. Tu configure la première ligne du fichier config.toml comme suit : baseurl = "/"
1. Tu démarre le serveur de test en local avec la commande : hugo server
1. Tu visualise le rendu à l'adresse : http://localhost:1313/
1. Le site ce paramètre dans le fichier config.toml
1. Ensuite, on peut modifier les fichiers qui correspondent chacun à une page dans le repertoire content/


## Pour déployer
1. Je suppose que tu as un compte framagit (ou gitlab). Ton compte s'appele MON_COMPTE. Nous allons y créer en ligne de commande le dépot MON_DEPOT.
1. Tu édite le fichier .gitlab-ci.yml en modifiant la ligne '- hugo' par '- hugo --baseURL https://MON_COMPTE.frama.io/MON_DEPOT/'
1. Tu versionne tout avec git : git add -A ; git commit -m "Initialisation de mon site avec hugo et gitlab-pages."
1. Tu ajoute ton dépot sur ton espace framagit (ou gitlab) : git remote add monDepot https://framagit.org/MON_COMPTE/MON_DEPOT.git (attention, si ton dépot s'appelle "monSite", l'URL se termine par "/monSite.git")
1. Tu créé ce nouveau dépôt MON_DEPOT sur ton espace : git push --set-upstream monDepot master
1. Ton site web est déjà en cours de déploiement. Tu peux suivre le pieline ici : https://framagit.org/MON_COMPTE/MON_DEPOT/pipelines
1. Quand tout est vert ton site est déployé ici : https://MON_COMPTE.frama.io/MON_DEPOT/


## Pour pousser des modifications
1. Tu modifie le fichier config.toml, les contenus dans le repertoire content/ dans lequel tu peux ajouter de nouvelles pages, et eventuellement le thème dans le repertoire themes/beautifulhugo/
1. Lorsque tu juge que c'est publiable, tu commit avec : git add -A ; git commit -m "MON_MESSAGE"
1. Tu publie avec la commande : git push


Existe t'il quelque chose d'une qualité pro aussi avancé plus simple ?

Pour aller plus loin :
* la doc d'hugo : https://gohugo.io/documentation/
* git + ssh
* changer le theme
* ajouter un nom de domaine par dessus les gitlab-pages
* ...
